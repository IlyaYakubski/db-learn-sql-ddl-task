CREATE SCHEMA mountain_schema;

CREATE TABLE mountain_schema.COUNTRIES (
    country_id SERIAL PRIMARY KEY,
    country_name VARCHAR(100) NOT NULL
);

CREATE TABLE mountain_schema.AREAS (
    area_id SERIAL PRIMARY KEY,
    area_name VARCHAR(100) NOT NULL
);

CREATE TABLE mountain_schema.MOUNTAINS (
    mountain_id SERIAL PRIMARY KEY,
    country_id INT REFERENCES mountain_schema.COUNTRIES(country_id),
    area_id INT REFERENCES mountain_schema.AREAS(area_id),
    mt_height INT CHECK (mt_height > 0),
    mt_name VARCHAR(100) NOT NULL
);

CREATE TABLE mountain_schema.CLIMBS (
    climb_id SERIAL PRIMARY KEY,
    mountain_id INT REFERENCES mountain_schema.MOUNTAINS(mountain_id),
    begin_time TIMESTAMP CHECK (begin_time > '2000-01-01'),
    end_time TIMESTAMP CHECK (end_time > begin_time)
);

CREATE TABLE mountain_schema.CLIMBERS (
    climber_id SERIAL PRIMARY KEY,
    cl_name VARCHAR(100) NOT NULL,
    cl_address VARCHAR(255)
);

CREATE TABLE mountain_schema.CLIMB_PARTICIPANTS (
    participant_id SERIAL PRIMARY KEY,
    climb_id INT REFERENCES mountain_schema.CLIMBS(climb_id),
    climber_id INT REFERENCES mountain_schema.CLIMBERS(climber_id)
);

CREATE TABLE mountain_schema.LEADERS (
    leader_id SERIAL PRIMARY KEY,
    lead_name VARCHAR(100) NOT NULL,
    lead_address VARCHAR(255)
);

CREATE TABLE mountain_schema.CLIMBS_OF_LEADERS (
    leaderClimb_id SERIAL PRIMARY KEY,
    climb_id INT REFERENCES mountain_schema.CLIMBS(climb_id),
    leader_id INT REFERENCES mountain_schema.LEADERS(leader_id)
);

ALTER TABLE mountain_schema.MOUNTAINS ADD CONSTRAINT unique_mt_name UNIQUE (mt_name);
ALTER TABLE mountain_schema.COUNTRIES ADD CONSTRAINT unique_country_name UNIQUE (country_name);
ALTER TABLE mountain_schema.AREAS ADD CONSTRAINT unique_area_name UNIQUE (area_name);
ALTER TABLE mountain_schema.CLIMBERS ADD CONSTRAINT unique_cl_name UNIQUE (cl_name);
ALTER TABLE mountain_schema.LEADERS ADD CONSTRAINT unique_lead_name UNIQUE (lead_name);

INSERT INTO mountain_schema.COUNTRIES (country_id, country_name) VALUES
   (1, 'Nepal'),
   (2, 'US'),
   (3, 'Korea');
   
INSERT INTO mountain_schema.AREAS (area_id, area_name) VALUES
   (1, 'Himalayas'),
   (2, 'Colorado'),
   (3, 'Ryanggang pr.');

INSERT INTO mountain_schema.MOUNTAINS (country_id, area_id, mt_height, mt_name) VALUES
    (1, 1, 8848, 'Everest'),
    (2, 2, 4372, 'La Plata'),
    (3, 3, 2744, 'Paektusan'),
	(2, 2, 4328, 'Yale');
   
INSERT INTO mountain_schema.CLIMBS (climb_id, mountain_id, begin_time, end_time) VALUES
   (1, 1, '2015-03-28', '2015-04-05'),
   (2, 2, '2010-07-07', '2010-07-10'),
   (3, 3, '2012-02-16', '2012-02-17'),
   (4, 4, '2006-12-31', '2007-01-02');
   
INSERT INTO mountain_schema.CLIMBERS (cl_name, cl_address) VALUES
   ('Li Jong-Chol', 'Tanchon, Inhung st.32, 24'),
   ('Hans Milke', 'Berlin, Fredersdorfer st.1, 14'),
   ('Jeremy Brown', 'Denver, Lincoln st.270, 34'),
   ('Yuri Tokarev', 'Bobrujsk, Rechnaya st.6'),
   ('Kyle Smith', 'Denver, S Ohio st.800'),
   ('Kim Hae-Rim', 'Samjiyong, Chollima st.9, 12'),
   ('James Harrington', 'Durango, W 16th st, 1604'),
   ('Pak San-Ok', 'Daegu, Janggi st.101, 78'),
   ('Andrzej Boniecki', 'Chorzow, Powstancow st.22, 5'),
   ('Richard White', 'Dallas, Pueblo st. 3431'),
   ('Houjou Satoshi', 'Hinamizawa, Iijima st.125');
   
INSERT INTO mountain_schema.CLIMB_PARTICIPANTS (climb_id, climber_id) VALUES
    (1, 2),
	(1, 4),
	(1, 11),
	(2, 3),
	(2, 5),
	(2, 7),
	(3, 1),
	(3, 6),
	(3, 8),
	(4, 9),
	(4, 10),
	(4, 11);
	
INSERT INTO mountain_schema.LEADERS (lead_name, lead_address) VALUES
   ('Kyle Smith', 'Denver, S Ohio st.800'),
   ('Li Jong-Chol', 'Tanchon, Inhung st.32, 24'),
   ('Houjou Satoshi', 'Hinamizawa, Iijima st.125');

INSERT INTO mountain_schema.CLIMBS_OF_LEADERS (climb_id, leader_id) VALUES
   (1, 3),
   (2, 1),
   (3, 2),
   (4, 3);

ALTER TABLE mountain_schema.MOUNTAINS 
   ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp NOT NULL;
ALTER TABLE mountain_schema.COUNTRIES 
   ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp NOT NULL;
ALTER TABLE mountain_schema.AREAS
   ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp NOT NULL;
ALTER TABLE mountain_schema.CLIMBS
   ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp NOT NULL;
ALTER TABLE mountain_schema.CLIMBERS
   ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp NOT NULL;
ALTER TABLE mountain_schema.CLIMB_PARTICIPANTS
   ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp NOT NULL;
ALTER TABLE mountain_schema.LEADERS
   ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp NOT NULL;
ALTER TABLE mountain_schema.CLIMBS_OF_LEADERS
   ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp NOT NULL;
   